/*PROYECTO ELO323 S2-2022
Sebastian Castillo
Sebastian Madariaga
Darael Badilla
*/

//Sintaxis ./Reciever <server> 

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> 
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORTNUMBER  47204 //PUERTO QUE RECIBE VIDEO UDP DE SERVIDOR REPEATER
#define PORTNUMBER_2 12345 //PUERTO PLAYOUT 
#define MAXLINE 64000

int main(int argc, char *argv[])
{
	int sockfd, sockfd_2;
	
	char buffer[MAXLINE];
	char hostname[64];
	char *hello = "Ping";
	
	int n, len, n_2, len_2;
	
	struct hostent *hp;
	struct sockaddr_in servaddr, servaddr_2, cli_addr;

	strcpy(hostname, argv[1]);
	hp = gethostbyname(hostname);

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	sockfd_2 = socket(AF_INET, SOCK_DGRAM, 0);

	memset(&servaddr, 0, sizeof(servaddr));
	memset(&servaddr_2, 0, sizeof(servaddr_2));
	memset(&cli_addr, 0, sizeof(cli_addr));

	servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORTNUMBER);
  memcpy(&servaddr.sin_addr, hp->h_addr_list[0], hp->h_length);

  servaddr_2.sin_family = AF_INET;
  servaddr_2.sin_port = htons(PORTNUMBER_2);
  servaddr_2.sin_addr.s_addr = inet_addr("10.2.50.11");//Ip playout

  len = sizeof(servaddr);
  len_2 = sizeof(servaddr_2);
  int len_3 = sizeof(cli_addr);

  sendto(sockfd, (char *)hello, strlen(hello), 0, (struct sockaddr *) &servaddr, len);
  while ((n = recvfrom(sockfd, (char *)buffer, MAXLINE, 0, (struct sockaddr *) &cli_addr, &len_3))>0){
    buffer[n] = '\0';
    sendto(sockfd_2, buffer, n, 0, (struct sockaddr *) &servaddr_2, len_2); //Manda video a Server Playout
    sendto(sockfd, (char *)hello, strlen(hello), 0, (struct sockaddr *) &servaddr, len);//Manda ping
  }
    
  close(sockfd);
  exit(0);

	return 0;
}
