/*PROYECTO ELO323 S2-2022
Sebastian Castillo
Sebastian Madariaga
Darael Badilla
*/

//Sintaxis ./Repeater

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>

/*PUERTOS DISPONIBLES DE ARAGORN PARA ESTUDIANTES {47200, 47201, 47202, 47203, 47204}*/

#define PORTNUMBER  47203 //PUERTO DE ESCUCHA DE VIDEO 
#define PORTNUMBER2 47204 //PUERTO DE REENVIO VIDEO A PLAYOUT
#define MAXLINE 64000 //MAX SIZE UDP PACKET

int main(int argc, char *argv[])
{
	int sockfd, sockfd_2;
	char buffer[MAXLINE];
    char buffer_2[MAXLINE];

    struct sockaddr_in serv_addr, cli_addr;
    struct sockaddr_in serv_addr_2, cli_addr_2;

    int n, len;
    int n_2, len_2;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    sockfd_2 = socket(AF_INET, SOCK_DGRAM, 0);

    memset(&serv_addr, 0, sizeof(serv_addr));
    memset(&serv_addr_2, 0, sizeof(serv_addr_2));

    memset(&cli_addr, 0, sizeof(cli_addr));
    memset(&cli_addr_2, 0, sizeof(cli_addr_2));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORTNUMBER);
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    serv_addr_2.sin_family = AF_INET;
    serv_addr_2.sin_port = htons(PORTNUMBER2);
    serv_addr_2.sin_addr.s_addr = htonl(INADDR_ANY);

    bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
    bind(sockfd_2, (struct sockaddr *) &serv_addr_2, sizeof(serv_addr_2));

    len = sizeof(cli_addr);
    len_2 = sizeof(cli_addr_2);

    while((n = recvfrom(sockfd, buffer, MAXLINE, 0, (struct sockaddr *)&cli_addr, &len)) > 0){ //Recibe video UDP
        buffer[n]='\0';
        
        n_2 =recvfrom(sockfd_2, buffer_2, MAXLINE, 0, (struct sockaddr*)&cli_addr_2, &len_2); //Recibe Hola de cliente playout
        buffer_2[n_2]='\0';

        sendto(sockfd_2, buffer, n, 0, (struct sockaddr*)&cli_addr_2, len_2); //Reenvia a socket de cliente playout paquete de video
    }

    close(sockfd);
    close(sockfd_2);
    exit(0);    


	return 0;
}
