PROYECTO ELO323 S2-2022
Sebastian Castillo
Sebastian Madariaga
Darael Badilla


=================CONTENIDO=================

*README.md: Archivo de texto que explica el contenido,
cómo preparar cada programa para su ejecución y cómo correrlos. 

*Repeater.c: Programa servidor UDP "Repeater" recibe video UDP y reenvía a cliente UDP "Reciever".

*Reciever.c: Programa cliente UDP "Reciever" recibe video UDP redireccionado por Repeater y posteriormente
envía a servidor playout.

===========================PREPARACIÓN================================

Forma de compilación de los programas:

Repeater.c -> $gcc Repeater.c -o Repeater.o

Reciever.c -> $gcc Reciever.c -o Reciever.o 

==================================EJECUCIÓN========================
Los programas se ejecutan de la siguiente forma:

*Repeater.c: 
$ ./Repeater.o 
	**Ej:$ ./Repeater.o 
 

*Reciever.c: 
$ ./Reciever.o <server> 
	**Ej:$ ./Reciever.o aragorn.elo.utfsm.cl


======CONSIDERACIONES=======

%%Repeater debe ser ejecutado en dispositivo intermediario. EJ: Servidor aragorn

%%Reciever debe ser ejecutado en dispositivo conectado a red de LAB TVD

 


